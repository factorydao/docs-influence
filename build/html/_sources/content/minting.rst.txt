Minting Identity
=================
.. |I| image:: ../images/influence_icon.png
    :height: 15px
    :target: https://financevote.readthedocs.io/projects/influence/en/latest/

Minting an identity is an anti-sybil protection.

| Learn more about `minting here <https://financevote.readthedocs.io/en/latest/content/whitepaper/13_digital_identity.html?#if-you-cant-beat-them-join-them>`_.

How does it work?
------------------
| In purpose of voting, first thing to do is to mint an identity in cost of burning some `$FVT <https://financevote.readthedocs.io/en/latest/content/FVT.html>`_ to gain voting power.
| And that is what it can be seen from user interface. But what is going on under the hood?

.. image:: ../images/MerkleIdentity.jpg

This scheme depicts mechanism standing behind minting an Identity within |I| **Influence.vote**.

|
| Our starting point is **GatedMerkleIdentity** contract, which remembers all Merkle trees root hashes and verifies whether Merkle proof delivered by user is in any of the Merkle trees. More on `Merkle tree here <https://financevote.readthedocs.io/en/latest/content/terminology/merkle_tree.html>`_.
| Adding tree hashes to **GatedMerkleIdentity** requires passing NFT address and gate address, both of which could be get via contracts.
|
| **GatedMerkleIdentity** connects to the contract of token, which is desired by user to get.
| In Influence.vote the only available token's contract right now is **VotingIdentity2**, which creates an Identity and sets NFT addresses for them, also it transfers NFT from one user to another and shows minter's NFT balance. More on `Identities here <https://financevote.readthedocs.io/en/latest/content/whitepaper/19_token_economics.html?#identity-tokens>`_.
|
| Price of minting an Identity is dynamic and determined by amount of Indentities already bought. That's why there are two price contracts. **FixedPriceGate** always returns basic price, while **LinearPriceGate** returns price increasing in time.
|
| **Incinerator** takes a token address and some ether. It uses the ether to purchase the token and burn them (remove them from supply permanently).
|
| The last two contracts indicated in the schema are used for trading from a smart contract.
| **UniswapRouter** and **SushiswapRouter** are contracts that works nearly the same and are used to safely swap tokens to and from different assets.
