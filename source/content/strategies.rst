Strategies
===========
| Influence supports various contracts, token standards and chains. Implemented different calculating methods called strategies help to handle such diversity and cover most user needs.
|
| Strategies used in the app are built out of params. Those 6 of them (listed in the next section) create 6 basic strategies:

.. csv-table:: 
   :header: "Strategy", "Base", "Voting", "Power Calculating"
   :widths: auto
   :align: left

   "Token weighted quadratic voting", "token-based", "quadratic", "not-aggregated"
   "Token weighted binary voting", "token-based", "binary", "not-aggregated"
   "Quadratic voting", "nft-based", "quadratic", "not-aggregated"
   "Binary voting", "nft-based", "binary", "not-aggregated"
   "Aggregated quadratic voting", "nft-based", "quadratic", "aggregated"
   "Aggregated binary voting", "nft-based", "binary", "aggregated"




| Any other strategies are custom and require a separate approach to meet even the highest user requirements.

Main params
------------
1. Base:

   -  **token-based**

      takes user balance of ERC20 token and grants voting power based on that amount

   -  **nft-based**

      grants voting power to those who own the expected NFT in their wallets
      
2. Voting method:

   - **binary**

      allows voting with the whole voting power at once on only one choice

   - **quadratic**

      allows voting on multiple choices while keeping the rule of quadratic voting at the same time (if you're not familiar with the idea, check it out `>here< <https://financevote.readthedocs.io/en/latest/content/analytics/report.html?highlight=quadratic#what-is-quadratic-voting>`_)

3. Vote power calculating method:

   - **aggregated**

      it allows voting per wallet, meaning the user might vote only once, using concatenated assets at once

   - **not-aggregated**

      it allows voting per NFT ID, meaning each identity might vote differently 


.. Strategies explained
.. ----------------------
.. #. Identity Quadratic Voting

..    | base: **nft-based**
..    | voting method: **quadratic**
..    | vote power calculating method: **not-aggregated**

..    | Strategy works with FVT Identities and is multichain (Ethereum and BSC). 
..    | contract address on ethereum: 0xf779cae120093807985d5F2e7DBB21d69be6b963
..    | contract address on binance smart chain: 0x951AED5E3554332BC2624D988c9c70d002D3Dba0

..    .. raw:: html

..       <details>
..       <summary><a>Expand to see advanced options</a></summary>

..    Advanced options::

..    .. raw:: html

..       </details>


.. #. Token weighted quadratic voting

..    | base: **token-based**
..    | voting method: **quadratic**
..    | vote power calculating method: **not-aggregated**

..    .. raw:: html

..       <details>
..       <summary><a>Expand to see advanced options</a></summary>

..    Advanced options::

..    .. raw:: html

..       </details>
      

.. #. Binary Voting

..    | base: **nft-based**
..    | voting method: **binary**
..    | vote power calculating method: **not-aggregated**

..    .. raw:: html

..       <details>
..       <summary><a>Expand to see advanced options</a></summary>

..    Advanced options::

..    .. raw:: html

..       </details>


.. #. Combined Chains

..    | base: **token-based**
..    | voting method: **quadratic**
..    | vote power calculating method: **aggregated**

..    .. raw:: html

..       <details>
..       <summary><a>Expand to see advanced options</a></summary>

..    Advanced options::

..    .. raw:: html

..       </details>

.. #. TotemFi QV

..    | base: **nft-based**
..    | voting method: **quadratic**
..    | vote power calculating method: **not-aggregated**

..    .. raw:: html

..       <details>
..       <summary><a>Expand to see advanced options</a></summary>

..    Advanced options::

..    .. raw:: html

..       </details>

.. #. Identity Binary Voting

..    | base: **nft-based**
..    | voting method: **binary**
..    | vote power calculating method: **not-aggregated**

..    .. raw:: html

..       <details>
..       <summary><a>Expand to see advanced options</a></summary>

..    Advanced options::

..    .. raw:: html

..       </details>

.. #. Aggregated Quadratic Voting / Quadratic Voting

..    | base: **nft-based**
..    | voting method: **quadratic**
..    | vote power calculating method: **aggregated**


..    .. raw:: html

..       <details>
..       <summary><a>Expand to see advanced options</a></summary>

..    Advanced options::

..    .. raw:: html

..       </details>


.. #. Aggregated Identity Binary Voting

..    | base: **nft-based**
..    | voting method: **binary**
..    | vote power calculating method: **aggregated**

..    .. raw:: html

..       <details>
..       <summary><a>Expand to see advanced options</a></summary>

..    Advanced options::

..    .. raw:: html

..       </details>


.. #. ETH based QV

..    | base: **token-based**
..    | voting method: **quadratic**
..    | vote power calculating method: **not-aggregated**

..    .. raw:: html

..       <details>
..       <summary><a>Expand to see advanced options</a></summary>

..    Advanced options::

..    .. raw:: html

..       </details>

.. #. Stake Weighted Vote

..    | base: **token-based**
..    | voting method: **quadratic**
..    | vote power calculating method: **not-aggregated**

..    .. raw:: html

..       <details>
..       <summary><a>Expand to see advanced options</a></summary>

..    Advanced options::

..    .. raw:: html

..       </details>

.. #. Fibonacci Weighted Vote

..    | base: **token-based**
..    | voting method: **quadratic**
..    | vote power calculating method: **not-aggregated**

..    .. raw:: html

..       <details>
..       <summary><a>Expand to see advanced options</a></summary>

..    Advanced options::

..    .. raw:: html

..       </details>

.. #. Aggregated Binary Voting

   | base: **nft-based**
   | voting method: **binary**
   | vote power calculating method: **aggregated**

   .. raw:: html

      <details>
      <summary><a>Expand to see advanced options</a></summary>

   Advanced options::

   .. raw:: html

      </details>



