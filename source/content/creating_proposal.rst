Creating Proposal
==================
.. |A| image:: ../images/hoverPR.png
    :height: 50px

.. |B| image:: ../images/hover.png
    :height: 50px

.. .. |space| image:: ../images/space.jpg

.. |question| image:: ../images/question.jpg
    :alt: question field
    :align: middle

.. |startdate| image:: ../images/startdate.jpg
   :width: 35%

.. |time| image:: ../images/time.jpg
   :width: 40%

.. |sign| image:: ../images/newsign.jpg
   :width: 46.5%

.. |signpicker| image:: ../images/signpicker.jpg
   :width: 40%

.. |signing| image:: ../images/signing.png
    :height: 500px

.. contents::

Entering New Proposal Mode
---------------------------
1. On the main page choose interesting you realm.
2. Hit ``New Proposal`` button above the proposals list (only moderators can add new proposal).

.. container:: box

    .. image:: ../images/proposal.gif
        :alt: entering new proposal creating form
        :width: 610px
        :align: center

  
Steps to Creating Proposal
---------------------------
| A quick reminder on how to properly set up a proposal. 
| If you are not familiar with any (or some) of the points, we encourage you to read more in the further parts of this article. 

1. Hit **New Proposal** button.
2. Fill in **title** ("Question" field) and **description** ("What is your question?"). You can use Markdown basic syntax to format your description (:doc:`read more <./Markdown>`).
3. Set **start** and **end** dates. 
4. Choose **strategy**.
5. Choose **proposal's tag**.
6. **Optional:** Decide on proposal's options (or leave them as are):
   
    1. Which chains should be allowed
    2. Turn on winnerbox
    3. Restrict viewing results
    4. Decide who is eligible to vote (whitelist) or/and who is ineligible (blacklist)

7. Fill in voting options (**choices**). You can choose to fill in **choices descriptions** as well. 
8. When everything is filled out we recommend to check everything because **once published there is no editing possibility**. You can decide to publish proposal in **draft mode**.
9.  After making sure everything is correct you can finally hit **Publish** button. If all required fields are filled in properly, *loading screen and signature request from MetaMask will appear*.
10. **All done!** Proposal has been made and you've been sent to the main page. Created proposal should appear on the top of the proposals list.
11. If you've created proposal in a **draft node**, decide on publishing it or deleting. 


Proposal elements
---------------------
.. container:: box
    
    .. container:: left
        
        When you first create a proposal You'll see this form:   
        
    .. container:: right
        
        |A|


.. container:: templ

    .. container:: templhover

        .. figure:: ../images/creating_proposal_1_proposal_main.png
            :target: https://financevote.readthedocs.io/projects/influence/en/latest/content/creating_proposal.html
            :width: 90%

    .. container:: templbasic

        .. figure:: ../images/creating_proposal_1_proposal.jpg
            :target: https://financevote.readthedocs.io/projects/influence/en/latest/content/creating_proposal.html
            :width: 90%

Proposal content
^^^^^^^^^^^^^^^^^
| Proposal content consists of the title ("Question") and the description ("What is your proposal?"). You can use Markdown basic syntax to format your description (:doc:`read more <./Markdown>`). 
| Below the question field there is a field reserved for a description of what your question concerns, try to clarify what you want to ask. You can also describe each choice you'll put to the vote so everyone knows what it is about. 
| If you will need more space for the text area, use the expand handle.
|

|B|


.. container:: templ

    .. container:: templhover

        .. figure:: ../images/creating_proposal_1_proposal_hover.jpg
            :width: 90%

    .. container:: templbasic

        .. figure:: ../images/creating_proposal_1_proposal_unhover.jpg
            :target: https://financevote.readthedocs.io/projects/influence/en/latest/content/creating_proposal.html
            :width: 90%

.. Note:: Remember to use less than 40 000 characters in your proposal description

Proposal voting choices
^^^^^^^^^^^^^^^^^^^^^^^^^
| Filling in at least 2 choices is mandatory. They should relate to the proposal description.
| To add more choices hit ``Add choice`` button, to remove choice use ``X`` mark next to the choice you want to remove. 
| **Adding a choice description is optional.** Descriptions are accessible through the voting component. Choices that have one are clickable and highlighting (check out the video for reference).

.. container:: video

    .. raw:: html

        <video controls src="../_static/choice_description.mp4" width="550"></video>  

Proposal details
^^^^^^^^^^^^^^^^^^^
| In the 'Action' field (Proposal's details) you decide when proposal starts and ends, on what kind of strategy it will run and what will be the proposal's tag. Below the graphic, you'll find a more specific description of each parameter. 

|B|


.. container:: templ

    .. container:: templhover

        .. figure:: ../images/creating_proposal_1_proposal_details.png
            :target: https://financevote.readthedocs.io/projects/influence/en/latest/content/creating_proposal.html
            :width: 90%

    .. container:: templbasic

        .. figure:: ../images/creating_proposal_1_proposal_unhover.jpg
            :target: https://financevote.readthedocs.io/projects/influence/en/latest/content/creating_proposal.html
            :width: 90%


| Mandatory proposal's elements are:

    - **start and end dates** (dates of voting start and end mustn't be the same)
  
        - |startdate| |time|
  
    - **strategy** - strategies are mechanics for proposals that determine the voting power of users and how they are allowed to cast their votes. Each strategy has different voting power calculations and relies on different sources (tokens, cryptocurrencies, NFTs). Each realm has a predefined set of strategies allowed to use in the proposals. 
    - **tag** - the proposal tag consists of up to 4 characters. Tags make it easier to find proposals in the list as well as categorize and organize them. 
        
        * **You can choose from existing tags within dropdown menu** or create your own by hitting "Add new sign". Choosing to add your own sign, a new window will pop up with name field and color picker.

        - |sign| |signpicker|

    - **snapshot** - it's the height of the block in the blockchain that was used to check account balance. 

| There are also a few additional setting options that could be used to control who can see and/or vote in the proposal and whether the proposal's winning option should be especially exposed after the end:

    - **acceptable chains** - as influence is a multichain platform, those checkboxes allow user to decide which chains should or shouldn't be considered within the proposal. Some of the strategies sums up user balance within all chains, so those checkboxes help control it. 
    - **winnerbox** - it's a field that appears at the proposal's end and contains the name of the choice (or choices) that has won.

    .. figure:: ../images/winnerbox.jpg
        :width: 70%

    .. figure:: ../images/3_way_tie.jpg
        :width: 70%

    - **viewing restriction** - it's an option that restricts the preview of voting results for those who didn't meet the prerequisites (having a specific token or NFT in the wallet, determined by the strategy)
    - **voting restrictions** are handled per chain (for example one address/token ID could be restricted only on mainnet but not on other chains). You can either type ID number/wallet address directly into the field or paste CSV formatted array:
    
        - **whitelist** - a list of IDs numbers or wallet addresses (**depending on strategy**) that are eligible to cast a vote:

        .. container:: video

            .. raw:: html

                <video controls src="../_static/whitelist.mp4" width="550"></video>  




        
        - **blacklist** - a list of IDs numbers or wallet addresses (**depending on strategy**) that are ineligible to cast a vote:

        .. container:: video

            .. raw:: html

                <video controls src="../_static/blacklist.mp4" width="550"></video>  


Draft mode
^^^^^^^^^^^^
| Before you publish your proposal you may want to share it with someone so they double check it. But since the proposal cannot be changed or deleted after publishing, you can publish a draft of it first.
| All you have to do to achieve that mode is to simply leave the draft switch 'On' during proposal creating:

.. image:: ../images/draft.jpg
    :width: 350px
    :align: center

| You may now share the draft with someone and be sure that no one will be able to vote while the proposal is in draft mode. 
|
| Once you'll decide that everything is correct, you can publish it (square with arrow icon) or if there were some mistakes, you can delete it (bin icon):

.. image:: ../images/draft_icons.jpg
    :width: 200px
    :align: center

| If you don't want to publish a proposal in a draft mode, remember to turn the draft switch 'Off' during proposal creation:

.. image:: ../images/draft_off.jpg
    :width: 350px
    :align: center









