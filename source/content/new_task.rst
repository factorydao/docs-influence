Create new task
================
.. image:: ../images/task_creation.jpg


| Form fields:

#. Title & description
#. Start & end dates 
#. Ranking end date - more on ranking further on
#. Max submissions length - how many symbols user may input into submission form
#. Task type - text, media, mixed (`more on task types <https://financevote.readthedocs.io/projects/influence/en/latest/content/submissions.html#text-submissions>`_`)
#. Tag - you can choose tag from the dropdown or create your own (`more on tags in the proposals view details <https://financevote.readthedocs.io/projects/influence/en/latest/content/creating_proposal.html?highlight=tag#proposal-details>`_)
#. 'Open to' box fields - decide how many submissions can be added (more on that further on)


Ranking
---------
| The ranking is a special task mode that starts right after the task end and lasts till the ranking end date set on task creation is reached. 
| From task start till the ranking end, users may vote on the submitted submissions to decide which are the best, but after the task end cannot add any new ones. So ranking mode is basically created to give chance to those submissions that were added near the task end, so they can gain some votes too. 
|
| Voting on submissions:

.. container:: video

    .. raw:: html

        <video controls src="../_static/voting_submission.mp4" width="550"></video>  

Submissions limits
-----------------------------
| On task creation you can decide how many submissions the user may add to the task and what kind of assets are allowed - NFT, Token or both.
|
| You cannot decide which NFT or which Tokens are allowed, you only decide on asset type. The system takes all assets that are allowed within that realm.
| For some realms, multiple strategies are implemented, and some of them may be using different NFTs. 
| Since tasks are insensitive to strategies, there is no way to whitelist or blacklist any specific NFTs contract addresses. 