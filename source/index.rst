.. influence.vote documentation master file, created by
   sphinx-quickstart on Fri Jun 25 13:46:58 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive

Welcome to influence.vote's documentation!
==========================================
`Influence`_ is designed for token weighted decentralised content curation and rough consensus formation around anything from tuning monetary policy parameters to meme markets. This is where DAO members deliberate and decide.

.. _Influence: https://influence.factorydao.org/

.. image:: ./images/influence_hz_b.png
   :height: 50px
   :alt: docs-influence missing
   :align: center
   :target: https://influence.factorydao.org/

.. toctree::
   :maxdepth: 2
   :caption: Back Room:
   :glob:

   /content/setting_up
   /content/minting

.. toctree::
   :maxdepth: 2
   :caption: For Voters:
   :glob:

   /content/voting
   /content/submissions

.. toctree::
   :maxdepth: 2
   :caption: For Admins and Members:
   :glob:

   /content/new_realm
   /content/creating_proposal
   /content/strategies
   /content/new_task
   /content/Markdown


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

Our products
===============

.. |A| image:: ./images/launch_icon.png
   :height: 15px
   :target: https://financevote.readthedocs.io/projects/launch/en/latest/
   
.. |B| image:: ./images/bank_icon_dark.png
   :height: 15px
   :target: https://financevote.readthedocs.io/projects/bank/en/latest/

.. |M| image:: ./images/markets_icon.png
   :height: 15px
   :target: https://financevote.readthedocs.io/projects/markets/en/latest/

.. |MI| image:: ./images/mint_icon.png
   :height: 15px
   :target: https://financevote.readthedocs.io/projects/mint/en/latest/
   
.. |Y| image:: ./images/yield_icon.png
   :height: 15px
   :target: https://financevote.readthedocs.io/projects/yield/en/latest/

* |A| `launch <https://financevote.readthedocs.io/projects/launch/en/latest/>`_
* |B| `bank <https://financevote.readthedocs.io/projects/bank/en/latest/>`_
* |M| `markets <https://financevote.readthedocs.io/projects/markets/en/latest/>`_
* |MI| `mint <https://financevote.readthedocs.io/projects/mint/en/latest/>`_
* |Y| `yield <https://financevote.readthedocs.io/projects/yield/en/latest/>`_

Back to main page
------------------
.. |H| image:: ./images/financevote_icon.png
   :height: 15px
   :target: https://financevote.readthedocs.io/en/latest/

* |H| `Home <https://financevote.readthedocs.io/en/latest/>`_

